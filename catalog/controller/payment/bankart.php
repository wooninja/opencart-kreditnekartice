<?php

/*
RewriteRule ^BankartResponse/?$ index.php?route=payment/bankart/response [L,QSA]
RewriteRule ^BankartAnswer/?$ index.php?route=payment/bankart/answer [L,QSA]
RewriteRule ^BankartError/?$ index.php?route=payment/bankart/error [L,QSA]
*/

class ControllerPaymentBankart extends Controller {

	public function index() {

		$this->load->language('payment/bankart');
		$this->load->model('checkout/order');
		$this->load->model('payment/bankart');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$data["shopUrl"] = $this->config->get('config_url');

		$data['ORDER_ID'] = $this->session->data['order_id'];
		$data['NAME1'] = $order_info['payment_firstname'];
		$data['NAME2'] = $order_info['payment_lastname'];
		$data['ADDR1'] = $order_info['payment_address_1'];
		$data['ADDR2'] = $order_info['payment_address_2'];
		$data['CITY'] = $order_info['payment_city'];
		$data['STATE'] = $order_info['payment_zone'];
		$data['ZIPCODE'] = $order_info['payment_postcode'];
		$data['COUNTRY'] = $order_info['payment_country'];
		$data['PHONE'] = $order_info['telephone'];
		$data['EMAIL'] = $order_info['email'];


		$data['APPROVED_URL'] = preg_replace("/^http:/i", "https:", $this->config->get('config_url') . "BankartAnswer" );  //$this->url->link('payment/bankart/answer', '', true);
		$data['RESPONSE_URL'] = preg_replace("/^http:/i", "https:", $this->config->get('config_url') . "BankartResponse");  //$this->url->link('payment/bankart/response', '', true);
		$data['DECLINED_URL'] = preg_replace("/^http:/i", "https:", $this->config->get('config_url') . "BankartError");  //$this->url->link('payment/bankart/error', '', true);

		$data['merchant'] = $this->config->get('bankart_merchant_id');

		$data["owner_email"] = $this->config->get('config_email');
		$data["apikey"] 	 = $this->config->get('bankart_api');

		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['text_loading'] = $this->language->get('text_loading');







		// Preko objekta v vticniku "e24PaymentPipe.php", vnese
		// trgovec podatke o nameravanem nakupu za namen inicializacije HPP strani.
		require_once dirname(__FILE__) . "/api/e24PaymentPipe.php";
		$paymentPipe = new e24PaymentPipe();

		// Pot do resource datoteke, ki jo trgovec dobi na svoji spletni strani sistema CGw. V njej
		// so zakodirani podatki potrebni za povezavo na CGw sistem. Ker se resource datoteka razpakira
		// v zacasno datoteko, morajo biti dovoljenja za mapo v kateri se nahaja resource datoteka
		// nastavljena polega branja tudi na pisanje.
		$paymentPipe->setResourcePath( dirname(__FILE__) . "/api/resource/" );
		// Alias na trgovcev terminal. Ta podatek se dobi na trgovcevi spletni strani v CGw.




		$paymentPipe->setAlias( $data['merchant'] );
		// Zajamemo POST podatke o transakciji in nastavitvi HPP strani.
		// Tip transakcije.
		$paymentPipe->setAction( 4 );
		// Vsota nakupa.
		$paymentPipe->setAmt( $this->currency->format($order_info['total'], $order_info['currency_code'], false, false) );

		// Denarna valuta, 978 - Euro.
		$paymentPipe->setCurrency( 978 );
		// Jezik, ki naj se prikaze na HPP strani.
		$paymentPipe->setLanguage( "sl_SI" );
		// Trgovcev URL naslov na katerega se poslje podatke o stanju placila potem, ko kupec vnese podatke o
		// placilni kartici na HPP strani.
		//$paymentPipe->setResponseURL( $this->config->get('config_url') . "BankartResponse" );
		$paymentPipe->setResponseURL( $data['RESPONSE_URL'] );
		// Trgovcev URL naslov na katerega se preusmeri kupca v primeru sistemske napake.
		$paymentPipe->setErrorURL( $data['DECLINED_URL'] );
		// Dodatna polja, ki treunto niso namenjena uporabi.
		$paymentPipe->setUdf2( $data['ORDER_ID'] );

		$paymentPipe->setTrackId(md5(uniqid()));

		// --------- Posljemo zahtevo po inicalizaciji nakupa oz. HPP strani. ------------------------
		if ($paymentPipe->performPaymentInitialization() != $paymentPipe->SUCCESS)  // neuspesna inicializacija
		{
		    // Izvede se preusmeritev na trgovcev URL o obvestilu o napaki.
		    $link = $paymentPipe->getErrorURL() . '?darn='.$paymentPipe->getAction().'&ErrorTx=' . $paymentPipe->getErrorMsg();

		}
		else  // uspesna inicializacija
		{
		    // Narocilo zapisemo v bazo.

		    /*add_post_meta( $order->id, "_tran_id_bankart", $paymentPipe->getTranID() ); // enolicen ID, ki je odvisen od casa kdaj je bila funkcija klicana
		    add_post_meta( $order->id, "_track_id_bankart", $paymentPipe->getTrackID() ); // enolicen ID, ki je odvisen od casa kdaj je bila funkcija klicana
		    add_post_meta( $order->id, "_payment_id_bankart", $paymentPipe->getPaymentId() ); // enolicen ID, ki je odvisen od casa kdaj je bila funkcija klicana*/

		    // Kupca preusmerimo na HPP stran, katere URL smo dobili iz inicializacijskega sporocila.
		    $link = $paymentPipe->getPaymentPage() . '?PaymentID=' . $paymentPipe->getPaymentId();

		}


		$data["URL"] = $link;

		///print_r($paymentPipe);

		return $this->load->view('payment/bankart', $data);

	}

	public function answer() {
		$this->load->language('payment/bankart');

		$this->load->model('checkout/order');

		$this->load->model('payment/bankart');


		// Preko objekta v vticniku "e24PaymentPipe.php", vnese
		// trgovec podatke o nameravanem nakupu za namen inicializacije HPP strani.
		require_once dirname(__FILE__) . "/api/e24PaymentPipe.php";


		$type = "4";

		$orderId = $_GET['order'];
		$order_info = $this->model_checkout_order->getOrder( $orderId );
		$amount = $this->currency->format($order_info['total'], $order_info['currency_code'], false, false);

		$orderTransaction = $this->model_payment_bankart->getOrder( $orderId );


		$_GET["resultcode"] = $orderTransaction["status_transakcija"];



		/// Če ja avtorizacija, moramo narediti še capture
		if ( $type == "4" ) {
		    
		    if ( $_GET["resultcode"]== "APPROVED" ) {
		       
		        $paymentPipe = new e24PaymentPipe();

		        // Pot do resource datoteke, ki jo trgovec dobi na svoji spletni strani sistema CGw. V njej
		        // so zakodirani podatki potrebni za povezavo na CGw sistem. Ker se resource datoteka razpakira
		        // v zacasno datoteko, morajo biti dovoljenja za mapo v kateri se nahaja resource datoteka
		        // nastavljena polega branja tudi na pisanje.
				$paymentPipe->setResourcePath( dirname(__FILE__) . "/api/resource/" );
		        // Alias na trgovcev terminal. Ta podatek se dobi na trgovcevi spletni strani v CGw.

				$ImeTerminala = $this->config->get('bankart_merchant_id');
		        $paymentPipe->setAlias( $ImeTerminala );
		        // Zajamemo POST podatke o transakciji in nastavitvi HPP strani.
		        // Tip transakcije.
		        $paymentPipe->setAction( "5" );
		        // Vsota nakupa.
		        $paymentPipe->setAmt( $amount );

		        


		        $paymentPipe->setPaymentId( $orderTransaction["_payment_id_bankart"] );
		        $paymentPipe->setTranID( $orderTransaction["_tran_id_bankart"] );
		        $paymentPipe->setTrackID( $orderTransaction["_track_id_bankart"] );


		        if ($paymentPipe->performPayment() != $paymentPipe->SUCCESS)
		        {
		        	$_GET["resultcode"] = "NOT CAPTURED";
		            //update_post_meta( $order->id, "status_transakcija", "NOT CAPTURED" );
		        }
		        else
		        {
		        	$_GET["resultcode"] = "CAPTURED";
		            //update_post_meta( $order->id, "status_transakcija", "CAPTURED" );
		        }


		    }

		}


		if ( ( $_GET["resultcode"]=="CAPTURED" || $_GET["resultcode"]=="APPROVED" ) )
		{
		  $this->model_checkout_order->addOrderHistory( $orderId, $this->config->get('bankart_status_complete'));
		}

		ob_start();

		?>

		<!doctype html>
		<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
		<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
		<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
		<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
		    <head>
		        <meta charset="utf-8">
		        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		        <title><?php echo $this->config->get('config_name');?></title>
		        <meta name="viewport" content="width=device-width, initial-scale=1">
		        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
		             

		        <script type="text/javascript">
		          WebFontConfig = {
		            google: { families: [ 'Roboto::latin,latin-ext' ] }
		          };
		          (function() {
		            var wf = document.createElement('script');
		            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		              '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		            wf.type = 'text/javascript';
		            wf.async = 'true';
		            var s = document.getElementsByTagName('script')[0];
		            s.parentNode.insertBefore(wf, s);
		          })(); </script>
		          <style>
		          body, a {
		            color: <?php echo $settings->text_color;?>;
		            font-family: 'Roboto', sans-serif;
		          }
		          a {
		            font-size: 12px;
		          }
		        </style>

		        <style>
		            body {
		                padding-top: 70px;
		                padding-bottom: 70px;
		            }
		        </style>

		        <?php
		        if (($_GET["resultcode"]=="CAPTURED" || 
		           $_GET["resultcode"]=="APPROVED"))
		        {
		        ?>
		         <meta http-equiv="refresh" content="4;URL='<?php echo $this->url->link('checkout/success', '', true); ?>'" />  
		        <? } else{


					$this->session->data['error'] = "Transaction failed!";

		        ?>
		         <meta http-equiv="refresh" content="4;URL='<?php echo $this->url->link('checkout/checkout', '', true); ?>'" />  
		        <?php
		        }
		        ?>

		    </head>
		    <body>



		    <div class="container">

		      <div class="row">
		        <div class="col-md-12">
		          
		            <?php
		            // get Merchant Notification parameters
		            $payID=$_GET["order"];

		            if (empty($payID))
		            {

		              header("Location: ". $this->config->get('config_url') . "BankartError" );
		            } 


		            if (($_GET["resultcode"]=="CAPTURED" || 
		               $_GET["resultcode"]=="APPROVED"))
		            {


		            ?>
		              <CENTER>
		                <FONT size="6" color="GREEN">
		                  Transaction successfull!
		                </FONT>
		              </CENTER>
		            <? }

		              else
		            {

					  $this->model_checkout_order->addOrderHistory( $orderId, $this->config->get('bankart_status_failed'));

		            ?>
		              <CENTER>
		                <FONT size="6" color="RED">
		                  Transaction failed!
		                </FONT>
		                <P>
		              </CENTER>
		            <? } ?>

		              <P><P>
		              

		        </div>
		      </div>
		    </div>



		    </body>
		</html>


		<?php

		$html = ob_get_clean();


		$this->response->setOutput( $html );

		/*$response_data = $this->request->post;

		if (isset( $response_data["ReturnOid"] )) {
			$order_info = $this->model_checkout_order->getOrder( $response_data["ReturnOid"] );

			if ($response_data['Response'] == 'Approved') {


				$this->model_checkout_order->addOrderHistory($response_data["ReturnOid"], $this->config->get('bankart_status_complete'));

				$this->response->redirect($this->url->link('checkout/success', '', true));

			} else {

				$this->model_checkout_order->addOrderHistory($response_data["ReturnOid"], $this->config->get('bankart_status_failed'));

				$this->session->data['error'] = "Transaction failed!";

				$this->response->redirect($this->url->link('checkout/checkout', '', true));

			}
		} else {
			$this->response->redirect($this->url->link('account/login', '', true));
		}*/


	}

	public function error() {
		$this->load->language('payment/bankart');

		$this->load->model('checkout/order');

		$this->load->model('payment/bankart');

		if ( !isset( $_GET["ErrorTx"] ) ) {
			$err = "No error!";
		} else {
			$err = $_GET["ErrorTx"];
		}

		$this->response->setOutput( "Transaction error - failed! ∞∞∞∞ " . $err );

	}

	public function response() {



		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

		$this->load->language('payment/bankart');
		$this->load->model('checkout/order');
		$this->load->model('payment/bankart');


		    // Razred za delo z bazo narocil.

		    // Preverimo, ce je prislo pri placilu, do kake napake.
		    if (isset($_POST['Error']))
				$errMsg = $_POST['Error'];
			else 
				$errMsg = '';

			if ( ! isset( $_POST["udf2"] ) ) {
				$errMsg = "No transaction found";
			}
				
		    if ( $errMsg != '' )
		    {
				// Zajamemo podatke o stanju placila, ki jih sistem CGw poslje z metodo POST zato, da lahko zapisemo tip napake.
				if (isset($_POST['paymentid']))
					$paymentid  = $_POST['paymentid'];
				else
					$paymentid  = '';

					///$errText = "Dolzina je ". count( $_POST );

			        // Zapisemo URL povezavo na trgovcevo spletno stran na katero naj sistem CGw preusmeri kupca. Pozor: pred URL naslov je
			        // potrebno obvezno navesti niz "REDIRECT=" !!!
					//$url = "REDIRECT=".plugin_dir_url( __FILE__ ) . "PaymentError.php?ErrorTx=" . $errText;
					$url = "REDIRECT=".$this->config->get('config_url') . "BankartError?ErrorTx=" .  $errMsg;
			    

		    }
		    else
		    {
				// Zajamemo podatke o stanju placila, ki jih sistem CGw poslje z metodo POST. To se ne pomeni, da je bilo
				// placilo odobreno.
				if (isset($_POST['paymentid']))
					$paymentid    = $_POST['paymentid'];
				else
					$paymentid    = ''; 
				if (isset($_POST['result']))
					$result       = $_POST['result'];
				else
					$result       = '';
				if (isset($_POST['responsecode']))
					$responsecode = $_POST['responsecode'];
				else
					$responsecode = ''; 
				if (isset($_POST['postdate']))
					$postdate     = $_POST['postdate'];
				else
					$postdate     = ''; 
				if (isset($_POST['udf1']))
					$udf1         = $_POST['udf1'];
				else
					$udf1         = '';
				if (isset($_POST['udf2']))
					$udf2         = $_POST['udf2'];
				else
					$udf2         = '';
				if (isset($_POST['udf3']))
					$udf3         = $_POST['udf3'];
				else
					$udf3         = '';
				if (isset($_POST['udf4']))
					$udf4         = $_POST['udf4'];
				else
					$udf4         = '';
				if (isset($_POST['udf5']))
					$udf5         = $_POST['udf5'];
				else
					$udf5         = '';		
				if (isset($_POST['tranid']))
					$tranid       = $_POST['tranid'];
				else
					$tranid       = ''; 
				if (isset($_POST['auth']))
					$auth         = $_POST['auth'];
				else
					$auth         = '';		
				if (isset($_POST['trackid']))
					$trackid      = $_POST['trackid'];
				else
					$trackid      = '';
				if (isset($_POST['ref']))
					$reference    = $_POST['ref'];
				else
					$reference    = '';
				if (isset($_POST['eci']))
					$eci		=  $_POST['eci'];
				else
					$eci		= '';



		        $this->model_payment_bankart->addTransaction( $udf2, array(
		        	"status_transakcija" => $result,
		        	"_tran_id_bankart" => $tranid,
		        	"_track_id_bankart" => $trackid,
		        	"_payment_id_bankart" => $paymentid,
		        ) );

			

		        // Zapisemo URL povezavo na trgovcevo spletno stran na katero naj sistem CGw preusmeri kupca. Pozor: pred URL naslov je
		        // potrebno obvezno navesti niz "REDIRECT=" !!
		        //$url = "REDIRECT=".plugin_dir_url( __FILE__ ) . "PaymentReceipt.php?order=".$udf2."&paymentid=" . $paymentid;

		        //$url = "REDIRECT=".preg_replace("/^http:/i", "https:", $this->config->get('config_url') . "BankartAnswer?order=". $udf2 . "&paymentid=" .$paymentid );
		        $url = "REDIRECT=".preg_replace("/^http:/i", "https:", $this->url->link('payment/bankart/answer', 'order='.$udf2, 'paymentid='.$paymentid, true) );



		    }

		$this->response->setOutput( $url );

	}


}