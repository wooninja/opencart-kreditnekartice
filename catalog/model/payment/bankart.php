<?php

class ModelPaymentBankart extends Model {


	public function getMethod($address, $total) {
		$this->load->language('payment/bankart');

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone_to_geo_zone` WHERE geo_zone_id = '" . (int)$this->config->get('bankart_hosted_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('bankart_hosted_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code' => 'bankart',
				'title' => $this->language->get('text_title'),
				'terms' => '',
				'sort_order' => 1
			);
		}

		return $method_data;
	}


	public function getOrder($order_id) {

		$qry = $this->db->query("SELECT * FROM `" . DB_PREFIX . "bankart_order_transaction` WHERE `bankart_order_id` = '" . (int)$order_id . "' LIMIT 1");

		if ($qry->num_rows) {
			$order = $qry->row;
			$order['sm'] = true;

			return $order;
		} else {
			return false;
		}
	}

	public function addTransaction($bankart_order_id, $data ) {
		//$this->logger('$type:\r\n' . print_r($type, 1));
		//$this->logger('$total:\r\n' . print_r($total, 1));
		$this->db->query("INSERT INTO `" . DB_PREFIX . "bankart_order_transaction` 
			SET `bankart_order_id` = '" . (int)$bankart_order_id . "', 
			`date_added` = now(), 
			`status_transakcija` = '" . $this->db->escape( $data["status_transakcija"] ) . "', 
			`_tran_id_bankart` = '" . $this->db->escape( $data["_tran_id_bankart"] ) . "', 
			`_track_id_bankart` = '" . $this->db->escape( $data["_track_id_bankart"] ) . "', 
			`_payment_id_bankart` = '" . $this->db->escape( $data["_payment_id_bankart"] ) . "'
		");
	}


	/*public function addOrder($order_info, $response_data) {
		if ($this->config->get('bluepay_hosted_transaction') == 'SALE') {
			$release_status = 1;
		} else {
			$release_status = null;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "bluepay_hosted_order` SET `order_id` = '" . (int)$order_info['order_id'] . "', `transaction_id` = '" . $this->db->escape($response_data['RRNO']) . "', `date_added` = now(), `date_modified` = now(), `release_status` = '" . (int)$release_status . "', `currency_code` = '" . $this->db->escape($order_info['currency_code']) . "', `total` = '" . $this->currency->format($order_info['total'], $order_info['currency_code'], false, false) . "'");

		return $this->db->getLastId();
	}

	public function addTransaction($bluepay_hosted_order_id, $type, $order_info) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "bluepay_hosted_order_transaction` SET `bluepay_hosted_order_id` = '" . (int)$bluepay_hosted_order_id . "', `date_added` = now(), `type` = '" . $this->db->escape($type) . "', `amount` = '" . $this->currency->format($order_info['total'], $order_info['currency_code'], false, false) . "'");
	}

	public function logger($message) {
		if ($this->config->get('bluepay_hosted_debug') == 1) {
			$log = new Log('bluepay_hosted.log');
			$log->write($message);
		}
	}*/

}
