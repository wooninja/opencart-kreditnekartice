<?php
class ModelPaymentBankart extends Model {
	public function install() {

		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "bankart_order_transaction` (
			  `bankart_order_transaction_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `bankart_order_id` INT(11) NOT NULL,
			  `status_transakcija` VARCHAR(255) NOT NULL,
			  `_tran_id_bankart` VARCHAR(255) NOT NULL,
			  `_track_id_bankart` VARCHAR(255) NOT NULL,
			  `_payment_id_bankart` VARCHAR(255) NOT NULL,
			  `date_added` DATETIME NOT NULL,
			  PRIMARY KEY (`bankart_order_transaction_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");

	}

	public function getOrder($order_id) {

		$qry = $this->db->query("SELECT * FROM `" . DB_PREFIX . "bankart_order_transaction` WHERE `bankart_order_id` = '" . (int)$order_id . "' LIMIT 1");

		if ($qry->num_rows) {
			$order = $qry->row;
			$order['sm'] = true;

			return $order;
		} else {
			return false;
		}
	}

	public function addTransaction($bankart_order_id, $data ) {
		//$this->logger('$type:\r\n' . print_r($type, 1));
		//$this->logger('$total:\r\n' . print_r($total, 1));
		$this->db->query("INSERT INTO `" . DB_PREFIX . "bankart_order_transaction` 
			SET `bankart_order_id` = '" . (int)$bankart_order_id . "', 
			`date_added` = now(), 
			`status_transakcija` = '" . $this->db->escape( $data["status_transakcija"] ) . "', 
			`_tran_id_bankart` = '" . $this->db->escape( $data["_tran_id_bankart"] ) . "', 
			`_track_id_bankart` = '" . $this->db->escape( $data["_track_id_bankart"] ) . "', 
			`_payment_id_bankart` = '" . $this->db->escape( $data["_payment_id_bankart"] ) . "'
		");
	}


	public function uninstall() {
		//$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "bankart_order_transaction`;");
	}

	public function void($order_id) {
		/*$bluepay_hosted_order = $this->getOrder($order_id);

		if (!empty($bluepay_hosted_order) && $bluepay_hosted_order['release_status'] == 1) {

			$void_data = array();

			$void_data['MERCHANT'] = $this->config->get('bluepay_hosted_account_id');
			$void_data["TRANSACTION_TYPE"] = 'VOID';
			$void_data["MODE"] = strtoupper($this->config->get('bluepay_hosted_test'));
			$void_data["RRNO"] = $bluepay_hosted_order['transaction_id'];

			$void_data['APPROVED_URL'] = HTTP_CATALOG . 'index.php?route=payment/bluepay_hosted/adminCallback';
			$void_data['DECLINED_URL'] = HTTP_CATALOG . 'index.php?route=payment/bluepay_hosted/adminCallback';
			$void_data['MISSING_URL'] = HTTP_CATALOG . 'index.php?route=payment/bluepay_hosted/adminCallback';

			if (isset($this->request->server["REMOTE_ADDR"])) {
				$void_data["REMOTE_IP"] = $this->request->server["REMOTE_ADDR"];
			}

			$tamper_proof_data = $this->config->get('bluepay_hosted_secret_key') . $void_data['MERCHANT'] . $void_data["TRANSACTION_TYPE"] . $void_data["RRNO"] . $void_data["MODE"];

			$void_data["TAMPER_PROOF_SEAL"] = md5($tamper_proof_data);

			$this->logger('$void_data:\r\n' . print_r($void_data, 1));

			$response_data = $this->sendCurl('https://secure.bluepay.com/interfaces/bp10emu', $void_data);

			return $response_data;
		} else {
			return false;
		}*/
	}

	/*public function updateVoidStatus($bluepay_hosted_order_id, $status) {
		$this->logger('$bankart_order_id:\r\n' . print_r($bluepay_hosted_order_id, 1));
		$this->logger('$status:\r\n' . print_r($status, 1));
		$this->db->query("UPDATE `" . DB_PREFIX . "bluepay_hosted_order` SET `void_status` = '" . (int)$status . "' WHERE `bluepay_hosted_order_id` = '" . (int)$bluepay_hosted_order_id . "'");
	}*/


}